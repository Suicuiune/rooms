use color_eyre::eyre::Context;
use color_eyre::Result;
use db::run_migrations;
use diesel::sql_types::Integer;
use diesel::{AsExpression, Connection, Insertable, Queryable, Selectable, SqliteConnection};
use dotenvy::dotenv;
use serde::{Deserialize, Serialize};
use std::env;
use std::time::{Duration, SystemTime};
use tokio::select;
use tracing::{error, info};

use crate::db::db_insert;

mod db;
mod schema;

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression)]
#[diesel(sql_type = Integer)]
enum Status {
    #[serde(rename = "frei")]
    Free,
    #[serde(rename = "belegt")]
    Occupied,
    #[serde(rename = "WAAS")]
    Waas,
    #[serde(rename = "unbekannt")]
    Unknown,
    #[serde(other)]
    Other,
}

#[derive(Debug, Clone, Serialize, Deserialize, Insertable, Queryable, Selectable)]
#[diesel(table_name = crate::schema::rooms)]
struct Record {
    #[serde(rename = "raum_nr")]
    room_nr: i32,
    #[serde(rename = "status")]
    status: Status,
    #[serde(default, rename = "zeit")]
    time: i64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Data {
    #[serde(rename = "raeume")]
    records: Vec<Record>,
}

impl Data {
    pub fn set_times(&mut self, t: SystemTime) -> Result<()> {
        let secs = t.duration_since(SystemTime::UNIX_EPOCH)?.as_secs() as i64;
        self.records.iter_mut().for_each(|dp| dp.time = secs);
        Ok(())
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();
    color_eyre::install()?;
    dotenv().context(".env file not found")?;
    let mut conn = SqliteConnection::establish(&env::var("DATABASE_URL")?)?;
    run_migrations(&mut conn).map_err(|e| color_eyre::eyre::eyre!(e.to_string()))?;

    let (cancel_sender, mut cancel_receiver) = tokio::sync::mpsc::channel(1);
    ctrlc::set_handler(move || {
        cancel_sender.blocking_send(()).unwrap();
    })
    .expect("Could not set Ctrl-C handler.");

    // loop until interrupted
    loop {
        // do run
        let result = run(&mut conn).await;
        let recv = &mut cancel_receiver;
        // log errors
        if result.is_err() {
            error!("Encountered error {:?}.", result.unwrap_err());
        }
        // sleep for 5 minutes
        // use futures for this plz
        select! {
            _ = tokio::time::sleep(Duration::from_secs(60*5)) => { continue }
            _ = recv.recv() => { break }
        }
    }
    Ok(())
}

async fn run(conn: &mut SqliteConnection) -> Result<()> {
    let body = get_data().await?;
    let data = parse(body).await?;
    db_insert(conn, &data)?;
    info!("Inserted {} records.", data.records.len());
    Ok(())
}

async fn parse(s: String) -> Result<Data> {
    let mut data: Data = serde_json::from_str(&s)?;
    data.set_times(SystemTime::now())?;
    Ok(data)
}

async fn get_data() -> Result<String> {
    let body = reqwest::get("https://iris.asta.tum.de/api/")
        .await?
        .text()
        .await?;
    Ok(body)
}
