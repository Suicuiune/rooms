// @generated automatically by Diesel CLI.

diesel::table! {
    rooms (id) {
        id -> Integer,
        room_nr -> Integer,
        status -> Integer,
        time -> BigInt,
    }
}
