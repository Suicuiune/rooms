use color_eyre::Result;
use diesel::backend::Backend;
use diesel::deserialize::FromSql;
use diesel::serialize::{Output, ToSql};
use diesel::sql_types::Integer;
use diesel::sqlite::{Sqlite, SqliteBindValue};
use diesel::{insert_into, RunQueryDsl, SqliteConnection};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use std::error::Error;

use crate::{schema, Data, Status};

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations");

pub fn run_migrations(
    connection: &mut impl MigrationHarness<Sqlite>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    connection.run_pending_migrations(MIGRATIONS)?;
    Ok(())
}

pub fn db_insert(conn: &mut SqliteConnection, data: &Data) -> Result<()> {
    use schema::rooms::dsl::*;
    insert_into(rooms)
        .values(data.records.clone())
        .execute(conn)
        .map_err(|e| color_eyre::eyre::eyre!(e.to_string()))?;
    Ok(())
}

impl FromSql<Integer, Sqlite> for Status
where
    i32: FromSql<Integer, Sqlite>,
{
    fn from_sql(bytes: <Sqlite as Backend>::RawValue<'_>) -> diesel::deserialize::Result<Self> {
        let data = i32::from_sql(bytes)?;
        let res = match data {
            0 => Status::Free,
            1 => Status::Occupied,
            2 => Status::Waas,
            3 => Status::Unknown,
            4 => Status::Other,
            _ => return Err(color_eyre::eyre::eyre!("Invalid Status").into()),
        };
        Ok(res)
    }
}

impl ToSql<Integer, Sqlite> for Status
where
    i32: ToSql<Integer, Sqlite>,
{
    fn to_sql(&self, out: &mut Output<Sqlite>) -> diesel::serialize::Result {
        let res = match *self {
            Status::Free => 0,
            Status::Occupied => 1,
            Status::Waas => 2,
            Status::Unknown => 3,
            Status::Other => 4,
        };
        out.set_value::<SqliteBindValue>(res.into());
        Ok(diesel::serialize::IsNull::No)
    }
}
